import {connect} from 'react-redux'; 
import Audio from './../../components/userAction/audio';
import {getAppTheme} from './../../actions/loginAction';
import {saveAudio} from './../../actions/audioAction';

const mapStateToProps = (state) => {
	return {
		lang: state.homeReducer.lang,
		audioData: state.audioReducer.audioData,
		fetching: state.loginReducer.fetching,
		appTheme: state.loginReducer.appThemeList
	};
};
  
const mapDispatchToProps = (dispatch, props) => ({
	getApp:(baseUrl,getApp)=>{dispatch(getAppTheme(baseUrl,getApp))},
	saveAudio:(item,audio)=>{dispatch(saveAudio(item,audio))},
	//action >> value get from page component
	//add new function (from page component)
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Audio);
