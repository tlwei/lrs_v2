import {connect} from 'react-redux';
import AudioList from './../../components/userAction/audioList';
import {getAppTheme} from './../../actions/loginAction';
import {saveAudio} from './../../actions/audioAction';

const mapStateToProps = (state) => {
	return {
		lang: state.homeReducer.lang,
		audioFetching: state.audioReducer.fetching,
		audioData: state.audioReducer.audioData,		
		fetching: state.loginReducer.fetching,
		appTheme: state.loginReducer.appThemeList
	};
};
  
const mapDispatchToProps = (dispatch, props) => ({
	getApp:(baseUrl,getApp)=>{dispatch(getAppTheme(baseUrl,getApp))},
	saveAudio:(item,audio,delete_status)=>{dispatch(saveAudio(item,audio,delete_status))},
	//action >> value get from page component
	//add new function (from page component)
});
  
export default connect(mapStateToProps, mapDispatchToProps)(AudioList);
