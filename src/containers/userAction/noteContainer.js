import {connect} from 'react-redux';
import Note from './../../components/userAction/note';
import {getAppTheme} from './../../actions/loginAction';
import {saveNote} from './../../actions/noteAction';

const mapStateToProps = (state) => {
	return {
		lang: state.homeReducer.lang,
		fetching: state.loginReducer.fetching,
		appTheme: state.loginReducer.appThemeList
	};
};
  
const mapDispatchToProps = (dispatch, props) => ({
	getApp:(baseUrl,getApp)=>{dispatch(getAppTheme(baseUrl,getApp))},
	saveNote:(item,note)=>{dispatch(saveNote(item,note))},
	//action >> value get from page component
	//add new function (from page component)
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Note);
