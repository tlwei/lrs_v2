import {connect} from 'react-redux';
import NoteList from './../../components/userAction/noteList';
import {getAppTheme} from './../../actions/loginAction';
import {saveNote} from './../../actions/noteAction';

const mapStateToProps = (state) => { 
	return {
		lang: state.homeReducer.lang,
		noteFetching: state.noteReducer.fetching,
		noteData: state.noteReducer.noteData,		
		fetching: state.loginReducer.fetching,
		appTheme: state.loginReducer.appThemeList
	};
};  
  
const mapDispatchToProps = (dispatch, props) => ({
	getApp:(baseUrl,getApp)=>{dispatch(getAppTheme(baseUrl,getApp))},
	saveNote:(item,note,delete_status)=>{dispatch(saveNote(item,note,delete_status))},
	//action >> value get from page component
	//add new function (from page component)
});
  
export default connect(mapStateToProps, mapDispatchToProps)(NoteList);
