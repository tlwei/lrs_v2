import {connect} from 'react-redux';
import Ticked from './../../components/userAction/ticked';
import {getUser} from './../../actions/loginUserAction';
import {getTicked} from './../../actions/tickedAction';
import {saveAction} from './../../actions/tickedAction';

const mapStateToProps = (state) => {
	return {
		lang: state.homeReducer.lang, 
		tickFetching:state.tickedReducer.fetching,
		tickedData: state.tickedReducer.tickedData,
		fetching: state.loginReducer.fetching,
		appTheme: state.loginReducer.appThemeList,
	};
};
  
const mapDispatchToProps = (dispatch, props) => ({
	saveAction:(item,note)=>{dispatch(saveAction(item,note))},
	getUser:(user,password,update)=>{dispatch(getUser(user,password,update))},
	getTicked:(tickedData,uid,scan_uid)=>{dispatch(getTicked(tickedData,uid,scan_uid))}
	//action >> value get from page component
	//add new function (from page component)
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Ticked);
