import {connect} from 'react-redux';
import Qrcode from './../../components/userAction/qrcode';
import {getAppTheme} from './../../actions/loginAction';
import {getScanUser} from './../../actions/qrcodeAction'
import {getUser} from './../../actions/loginUserAction'

const mapStateToProps = (state) => {
	return {
		lang: state.homeReducer.lang,
		scanUser:state.qrcodeReducer.scanUserData,
		user:state.loginUserReducer.userData,
		fetching: state.loginReducer.fetching,
		appTheme: state.loginReducer.appThemeList
	};
}; 
  
const mapDispatchToProps = (dispatch, props) => ({
	getApp:(baseUrl,getApp)=>{dispatch(getAppTheme(baseUrl,getApp))},
	getUserId:(userId)=>{dispatch(getUserId(userId))},
	getScanUser:(userId,scanId,app_url)=>{dispatch(getScanUser(userId,scanId,app_url))},
	getUser:(user,password,update,device_id,device_name)=>{dispatch(getUser(user,password,update,device_id,device_name))},
	//action >> value get from page component
	//add new function (from page component)
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Qrcode);
