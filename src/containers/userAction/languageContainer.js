import {connect} from 'react-redux';
import Language from './../../components/userAction/language';
import { getLang } from "./../../actions/homeAction";
const mapStateToProps = (state) => {
	return {
		lang: state.homeReducer.lang,
	};
};
  
const mapDispatchToProps = (dispatch, props) => ({
	getLang:(lang)=>{dispatch(getLang(lang))},
	//action >> value get from page component
	//add new function (from page component)
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Language);
