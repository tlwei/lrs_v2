import { connect } from 'react-redux';
import Home from './../../components/application/home';
import {getAppTheme} from './../../actions/loginAction';
import {logoutUser} from './../../actions/loginUserAction'


const mapStateToProps = (state) => {
	return {
		lang: state.homeReducer.lang,
		user:state.loginUserReducer.userData,
		fetching: state.loginReducer.fetching,
		appTheme: state.loginReducer.appThemeList
	};
};
  
const mapDispatchToProps = (dispatch, props) => ({
	getApp:(baseUrl,getApp)=>{dispatch(getAppTheme(baseUrl,getApp))},
	logoutUser:()=>{dispatch(logoutUser())}
	//action >> value get from page component
	//add new function (from page component)
});
  

export default connect(mapStateToProps, mapDispatchToProps)(Home);
