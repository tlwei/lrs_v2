import { connect } from 'react-redux';
import HotLeads from './../../components/application/hotLead';
import {getAppTheme} from './../../actions/loginAction';
import {saveNote} from './../../actions/noteAction';
import {saveAudio} from './../../actions/audioAction';
import {saveAction} from './../../actions/tickedAction';

const mapStateToProps = (state) => {
	return {
		lang: state.homeReducer.lang,
		user:state.loginUserReducer.userData,
		fetching: state.loginReducer.fetching,
		appTheme: state.loginReducer.appThemeList
	};
};
  
const mapDispatchToProps = (dispatch, props) => ({
	getApp:(baseUrl,getApp)=>{dispatch(getAppTheme(baseUrl,getApp))},
	saveAction:(item,note)=>{dispatch(saveAction(item,note))},
	saveNote:(item,note)=>{dispatch(saveNote(item,note))},
	saveAudio:(item,note)=>{dispatch(saveAudio(item,note))},
	//action >> value get from page component
	//add new function (from page component)
});
  

export default connect(mapStateToProps, mapDispatchToProps)(HotLeads);
