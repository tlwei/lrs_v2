import { connect } from 'react-redux';
import scannedLeads from './../../components/application/scannedLeads';
import {getAppTheme} from './../../actions/loginAction';

const mapStateToProps = (state) => {
	return { 
		lang: state.homeReducer.lang,
		user:state.loginUserReducer.userData,
		fetching: state.loginReducer.fetching,
		appTheme: state.loginReducer.appThemeList
	};
};
  
const mapDispatchToProps = (dispatch, props) => ({
	getApp:(baseUrl,getApp)=>{dispatch(getAppTheme(baseUrl,getApp))},
	//action >> value get from page component
	//add new function (from page component)
});
  

export default connect(mapStateToProps, mapDispatchToProps)(scannedLeads);
