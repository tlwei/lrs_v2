import { connect } from 'react-redux';
import loginPage from '../../components/userComponent/loginPage';
import {getAppTheme} from './../../actions/loginAction';
import {getUser,logoutUser} from './../../actions/loginUserAction'
import { getLang } from "./../../actions/homeAction";
const mapStateToProps = (state) => {
	return {	
		lang: state.homeReducer.lang,
		user:state.loginUserReducer.userData,
		loginFetch: state.loginUserReducer.fetching,
		fetching: state.loginReducer.fetching,
		appTheme: state.loginReducer.appThemeList
	};
}; 

const mapDispatchToProps = (dispatch, props) => ({
	getLang:(lang)=>{dispatch(getLang(lang))},
	getApp:(baseUrl,getApp)=>{dispatch(getAppTheme(baseUrl,getApp))},
	getUser:(user,password,update,device_id,device_name)=>{dispatch(getUser(user,password,update,device_id,device_name))},
	logoutUser:()=>{dispatch(logoutUser())}
});
  

export default connect(mapStateToProps, mapDispatchToProps)(loginPage);
