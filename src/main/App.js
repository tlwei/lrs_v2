import React, { Component } from 'react';
import {Root, Container} from 'native-base';
import codePush from 'react-native-code-push';
import { createAppContainer } from 'react-navigation';
import { AppSwitchNavigator } from '../navigator/mainNavigator/screenNavigator'
import { YellowBox,NetInfo } from 'react-native';
import {Provider} from 'react-redux'
import { Toast } from 'native-base';
import  {store}  from './../store/index'
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
import OfflineNotice from './../common/noInternet'


//store

class App extends Component {
	constructor (props) {
		super(props);
		this.state = {
			checkText:'',
			checkUpdate:false,
			isConnected: true,
			
		};
	}
	///code push 
	codePushStatusDidChange(status) {
		console.log("in")
		switch (status) {
			case codePush.SyncStatus.CHECKING_FOR_UPDATE:
				console.log("Checking for updates.");
			
				//codePush.clearUpdates();
				break;
			case codePush.SyncStatus.DOWNLOADING_PACKAGE:
				console.log("Downloading Package...");
				
				Toast.show({
					text: "Downloading Package.",
					duration: 2000,
					position: "top"
				});
				break;
		case codePush.SyncStatus.INSTALLING_UPDATE:
			console.log("Installing update.");
			Toast.show({
				text: "Installing update.",
				duration: 2000,
				position: "top"
			});
			break;
			case codePush.SyncStatus.UP_TO_DATE:
				console.log("Up-to-date.");
				break;
			case codePush.SyncStatus.UPDATE_INSTALLED:
				console.log("Update installed.");
				Toast.show({
					text: "Update Finish, Restarting Application.",
					duration: 2000,
					position: "top"
				});
				codePush.restartApp(); 		
				break;
		}
	}
	codePushDownloadDidProgress(progress) {
		// console.log('code_push_progress:',progress);
	}

	componentDidMount() {
		
		NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
	}

	componentWillUnmount() {
		NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
	}

	handleConnectivityChange = isConnected => {
		this.setState({ isConnected });
	}

	render() {
		return (
			<Provider store={store}>
				<Root>
					{!this.state.isConnected && <OfflineNotice />}
					<AppContainer />
				</Root>
			</Provider>
		);	
	}
}

let codePushOptions = {
	checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
	mandatoryInstallMode: codePush.InstallMode.ON_NEXT_RESTART
};
// App = codePush(codePushOptions)(App);

export default App;

const AppContainer = createAppContainer(AppSwitchNavigator)