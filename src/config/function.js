import {baseUrl,methodUrl} from './../config/constants'

export function HelloChandu() {
    console.log("hello1")
    return "hello2"
}

export async function getUrl() {
    try {
        await fetch(`${baseUrl}${methodUrl}`, 
          {
            method: "GET",
            headers: {
              Accept: 'application/json',
            } 
          }) 
          .then((response) => response.json())
          .then(responseData =>{
              console.log("responseData",responseData)
          }).catch(e => {console.log("err",e)});
    }catch (error) {
        console.error(error);
    }
}