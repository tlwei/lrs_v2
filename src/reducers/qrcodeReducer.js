//initail
const initialState={
	User:{
        fetching: false,
        scanUserData: [],
        errorMessage: null
    },
}
//reducer
export default (state = initialState,action)=>{
	switch(action.type)
	{
		case 'GET_SCAN_USER_REQUESTING':
			return {
                fetching: true
            }
        case 'GET_SCAN_USER_SUCCESS':
			return {
                fetching: false,
                scanUserData:action.user
            }
		case 'GET_SCAN_USER_FAILED':
			return {
                fetching: true,
                errorMessage: action.errorMessage
            }
	}
	return state 
}