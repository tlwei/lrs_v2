//initail
const initialState={
	appTheme:{
        fetching: false,
        appThemeList: [],
        errorMessage: null
    },
}
//reducer
export default (state = initialState,action)=>{
	switch(action.type)
	{
		case 'GET_APP_REQUESTING':
			return {
                fetching: true
            }
		case 'GET_APP_SUCCESS':
			return {
                fetching: false,
                appThemeList:action.theme[0]
            }
		case 'GET_APP_FAILED':
			return {
                fetching: true,
                errorMessage: action.errorMessage
            }
	}
	return state 
}