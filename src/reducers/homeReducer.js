import ch from './../components/Language/ch.json'
import en from './../components/Language/en.json'
import { AsyncStorage } from "react-native";
//initail
const initialState={
	lang:'',
}
//reducer
export default (state = initialState,action)=>{

	switch(action.type)
	{
		case 'GET_CH':
			console.log('save ch')
			AsyncStorage.setItem('lang','中文')
			return {lang:ch}
		case 'GET_EN':
			console.log('save en')
			AsyncStorage.setItem('lang','en')
			return {lang:en}
	}
	return state 
}