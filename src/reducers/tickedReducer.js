//initail
const initialState={
	Ticked:{
        fetching: false,
        tickedData: [],
        errorMessage: null
    },
}
//reducer
export default (state = initialState,action)=>{
	switch(action.type)
	{
		case 'SAVE_ACTION_REQUESTING':
			return {
                fetching: true
            }
        case 'SAVE_ACTION_SUCCESS':
			return {
                fetching: false,
                tickedData:action.user
            }
		case 'SAVE_ACTION_FAILED':
			return {
                fetching: true,
                errorMessage: action.errorMessage
            }
	}
	return state 
}