import { combineReducers } from 'redux';
import homeReducer from './homeReducer';
import loginReducer from './loginReducer';
import loginUserReducer from './loginUserReducer';
import qrcodeReducer from './qrcodeReducer';
import noteReducer from './noteReducer';
import audioReducer from './audioReducer';
import tickedReducer from './tickedReducer';

export const reducer = combineReducers({
	homeReducer,
	loginReducer,
	loginUserReducer,
	qrcodeReducer,
	noteReducer,
	audioReducer,
	tickedReducer
});