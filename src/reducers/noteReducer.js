//initail
const initialState={
	Note:{
        fetching: false,
        noteData: [],
        errorMessage: null
    },
}
//reducer
export default (state = initialState,action)=>{
	switch(action.type)
	{
		case 'SAVE_NOTE_REQUESTING':
			return {
                fetching: true
            }
        case 'SAVE_NOTE_SUCCESS':
			return {
                fetching: false,
                noteData:action.user
            }
		case 'SAVE_NOTE_FAILED':
			return {
                fetching: true,
                errorMessage: action.errorMessage
            }
	}
	return state 
}