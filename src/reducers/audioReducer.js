//initail
const initialState={
	Audio:{
        fetching: false,
        audioData: [],
        errorMessage: null
    },
}
//reducer
export default (state = initialState,action)=>{
	switch(action.type)
	{
		case 'SAVE_AUDIO_REQUESTING':
			return {
                fetching: true
            }
        case 'SAVE_AUDIO_SUCCESS':
			return {
                fetching: false,
                audioData:action.user
            }
		case 'SAVE_AUDIO_FAILED':
			return {
                fetching: true,
                errorMessage: action.errorMessage
            }
	}
	return state 
}