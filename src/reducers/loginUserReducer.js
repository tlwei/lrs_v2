//initail
const initialState={
	User:{
        fetching: false,
        userData: [],
        errorMessage: null
    },
}
//reducer
export default (state = initialState,action)=>{
	switch(action.type)
	{
		case 'GET_USER_REQUESTING':
			return {
                fetching: true
            }
        case 'GET_LOGOUT_USER':
			return {
                fetching: false,
                userData:undefined
            }
        case 'GET_USER_SUCCESS':
			return {
                fetching: false,
                userData:action.user
            }
		case 'GET_USER_FAILED':
			return {
                fetching: true,
                errorMessage: action.errorMessage
            }
	}
	return state 
}