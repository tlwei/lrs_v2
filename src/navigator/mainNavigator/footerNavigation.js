////footer bar
import React, { Component } from "react";
import FooterImage from './../../common/footerImage'
import { ApplicationStack } from "../contentNavigator/applicationNavigator/homeNavigator"

import { createBottomTabNavigator } from "react-navigation"

export const DashboardTabNavigator = createBottomTabNavigator({
	ApplicationStack: {
		screen: ApplicationStack,
		navigationOptions: {
			tabBarLabel: ({tintColor}) => {
				return (<FooterImage />)
			},	
		},
	},
},{
	tabBarOptions: {
		style: {
			height:55
        },
    }
})