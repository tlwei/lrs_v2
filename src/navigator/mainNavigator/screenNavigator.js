import LoginScreen from '../../containers/userComponent/loginContainer'
import { DashboardTabNavigator } from '../../navigator/mainNavigator/footerNavigation'
import { createSwitchNavigator } from 'react-navigation';
import Qrcode from './../../containers/userAction/qrcodeContainer';
import Language from './../../containers/userAction/languageContainer';

export const AppSwitchNavigator = createSwitchNavigator({
	LoginScreen:LoginScreen,
	Qrcode:Qrcode,
	WelcomeScreen:DashboardTabNavigator,
	Language:Language
})