import React, { Component } from 'react'; 
import Application from './../../../containers/application/homeContainer';
import Ticked from './../../../containers/userAction/tickedContainer';
import Notes from './../../../containers/userAction/noteListContainer';
import NotesData from './../../../containers/userAction/noteContainer';
import Audio from './../../../containers/userAction/audioContainer';
import AudioList from './../../../containers/userAction/audioListContainer'
import Qrcode from './../../../containers/userAction/qrcodeContainer';
import IconDisplay from './../../../common/icon';
import {StyleSheet,TouchableOpacity,AsyncStorage} from 'react-native';
import ApplicationDetails from './../../../containers/application/scannedLeadsContainer';
import HotLeads from './../../../containers/application/hotLeadsContainer';
import { Icon } from 'native-base';
import { createStackNavigator } from 'react-navigation';
import ch from './../../../components/Language/ch.json'
import en from './../../../components/Language/en.json'

const styles = StyleSheet.create({
	header:{
		marginRight:50,
		textAlign: 'center',
		flexGrow:1,
		alignSelf:'center',
	}
})

/////Application page
 

const resetAction = AsyncStorage.getItem('lang').then((lang)=>{
	console.log('getlgg',lang)
	if(lang == '中文'){
		language = ch
	}else{
		language = en
	}
	return language
})


export const ApplicationStack = createStackNavigator({
	
	Application:{
		screen:Application,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:resetAction._55.Home[0],
				headerTitleStyle: {
					textAlign: 'center',
					flexGrow:1,
					alignSelf:'center',
				},
				
				headerLeft:(
					<TouchableOpacity onPress={()=>navigation.navigate('LoginScreen',{logout:true})}>
						<Icon style={{paddingLeft:15,width:90}} name='md-exit'/>
					</TouchableOpacity>
				),
				
				headerRight:(
					<TouchableOpacity style={{right:0,width:40}} onPress={()=>navigation.navigate('Qrcode',{status:'scan'})}>
						<Icon name='md-qr-scanner'/>
					</TouchableOpacity>
				),
			}
		}
	},
	Detail:{
		screen:ApplicationDetails,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:resetAction._55.ScanLeads[0],
				headerTitleStyle: styles.header,
				headerLeft:(
					<TouchableOpacity onPress={()=>navigation.goBack()}>
						<Icon style={{paddingLeft:15,width:90}} name='ios-arrow-back'/>
					</TouchableOpacity>
				)
			}
		}
	},
	HotLeads:{
		screen:HotLeads,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:resetAction._55.HotLeads[0],
				headerTitleStyle: styles.header,
				headerLeft:(
					<TouchableOpacity onPress={()=>navigation.goBack()}>
						<Icon style={{paddingLeft:15,width:90}} name='ios-arrow-back'/>
					</TouchableOpacity>
				)
			}
		}
	},
	Ticked:{
		screen:Ticked,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:resetAction._55.Action[0],
				headerTitleStyle: styles.header,
				headerLeft:(
					<TouchableOpacity onPress={()=>navigation.goBack()}>
						<Icon style={{paddingLeft:15,width:90}} name='ios-arrow-back'/>
					</TouchableOpacity>
				)
			}
		}
	},
	Notes:{
		screen:Notes,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:resetAction._55.Notes[0],
				headerTitleStyle: styles.header,
				headerLeft:(
					<TouchableOpacity onPress={()=>navigation.goBack()}>
						<Icon style={{paddingLeft:15,width:90}} name='ios-arrow-back'/>
					</TouchableOpacity>
				)
			}
		}
	},
	NotesData:{
		screen:NotesData,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:resetAction._55.Notes[1],
				headerTitleStyle: styles.header,
				headerLeft:(
					<TouchableOpacity onPress={()=>navigation.goBack()}>
						<Icon style={{paddingLeft:15,width:90}} name='ios-arrow-back'/>
					</TouchableOpacity>
				)
			}
		}
	},
	Audio:{
		screen:Audio,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:resetAction._55.Audio[0],
				headerTitleStyle: styles.header,
				headerLeft:(
					<TouchableOpacity onPress={()=>navigation.goBack()}>
						<Icon style={{paddingLeft:15,width:90}} name='ios-arrow-back'/>
					</TouchableOpacity>
				),
				headerRight:(
					<TouchableOpacity style={{right:0,width:40}} onPress={()=>navigation.navigate('AudioList')}>
						<Icon name='md-menu'/>
					</TouchableOpacity>
				),
			}
		}
	},AudioList:{
		screen:AudioList,
		navigationOptions:({navigation})=>{
			return {
				headerTitle:resetAction._55.Audio[6],
				headerTitleStyle: styles.header,
				headerLeft:(
					<TouchableOpacity onPress={()=>navigation.goBack()}>
						<Icon style={{paddingLeft:15,width:90}} name='ios-arrow-back'/>
					</TouchableOpacity>
				),
			}
		}
	}
},{
	defaultNavigationOptions:{
		gesturesEnabled:true
	}
})
