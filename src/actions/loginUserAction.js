import { AsyncStorage } from 'react-native';

import {appUrl,baseUrl,loginUser} from './../config/constants'
import { Toast } from 'native-base';
export const getUser = (user,password,update,device_id,device_name) => async dispatch => {

    try {
        dispatch(getUserRequsting());
        console.log("1")
        // Toast.show({
        //   text: "success 1!",
        //   buttonText: "Done",
        //   type: "danger",
        //   position: "top",
        //   duration: 6000
        // })
          userData = await AsyncStorage.getItem('user') || user
          passwordData = await AsyncStorage.getItem('password') || password

        await fetch(`${baseUrl}${loginUser}`, 
        {
          method: "POST",
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
          },
          body:JSON.stringify({
            user:userData,
            password:passwordData,
            app_url:appUrl,
            device_id:device_id,
            device_name:device_name
          }) // <-- Post parameters        
        })
        .then((response) => response.json())
        .then((responseData) =>{
          // Toast.show({
          //   text: "success 3!",
          //   buttonText: "Done",
          //   type: "danger",
          //   position: "top",
          //   duration: 6000
          // })
          console.log('2')
          dispatch(getUserSuccess([responseData]));
        })
        
    } catch (e) {
      // Toast.show({
      //   text: "err 3!",
      //   buttonText: "Done",
      //   type: "danger",
      //   position: "top",
      //   duration: 6000
      // })
      console.log('3')
        dispatch(getUserFailed(e));
    }
}

export const logoutUser = () => async dispatch => {
  dispatch(getLogoutUser());
}

const getUserRequsting = () => ({
    type:'GET_USER_REQUESTING',
})
const getLogoutUser = () => ({
  type:'GET_LOGOUT_USER',
})
const getUserSuccess = (user) => ({
    type:'GET_USER_SUCCESS',
    user
})
const getUserFailed = (errorMessage) => ({
    type:'GET_USER_FAILED',
    errorMessage
})


