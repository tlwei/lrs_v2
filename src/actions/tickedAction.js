import {ticked,baseUrl,appUrl} from './../config/constants'
import { Toast } from 'native-base';
export const saveAction = (item,note) => async dispatch => {

    try {
      dispatch(saveActionRequsting());
      let noteBody = {
          uid:item.uid,
          scan_uid:item.scan_uid,
          app_url:item.app_url,
      }

      if(note !== undefined){
          noteBody = {
              uid:item.uid,
              scan_uid:item.scan_uid,
              app_url:item.app_url,
              action_list:item.action_list,
              status:note
          }

      }
      
      await fetch(`${baseUrl}${ticked}`, 
      {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
        },
        body:JSON.stringify(noteBody) // <-- Post parameters        
      })
      .then((response) => response.json())
      .then((responseData) =>{
        dispatch(saveActionSuccess(responseData));
      })
      
  } catch (e) {
      dispatch(saveActionFailed(e));
  }
}

const saveActionRequsting = () => ({
  type:'SAVE_ACTION_REQUESTING',
})
const saveActionSuccess = (user) => ({
  type:'SAVE_ACTION_SUCCESS',
  user
})
const saveActionFailed = (errorMessage) => ({
  type:'SAVE_ACTION_FAILED',
  errorMessage
})