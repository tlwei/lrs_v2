import {audioUrl,baseUrl,appUrl} from './../config/constants'
import { Toast } from 'native-base';
import {Platform} from 'react-native';
import {AudioRecorder, AudioUtils} from 'react-native-audio';
export const saveAudio = (item,audio,delete_status) => async dispatch => {
    
    try {
        dispatch(saveAudioRequsting());
        const formData = new FormData()
        let path = AudioUtils.DocumentDirectoryPath + '/test.aac';
        if(Platform.OS == 'android'){
            // Toast.show({
            //     text: "ANDROID!",
            //     buttonText: "Done",
            //     type: "success",
            //     position: "top",
            //     duration: 4000
            // })
            path = 'file://' + AudioUtils.DocumentDirectoryPath + '/test.aac'
        }
        
        let audioData = [['uid',item.uid],['scan_uid',item.scan_uid],['app_url',item.app_url]]
        console.log('item',audioData)
        if(audio !== undefined){
             
            console.log("audio,",audio)
            audioData = [['uid',item.uid],['scan_uid',item.scan_uid],['app_url',item.app_url],['audio_list',item.audio_list],['name',item.name]]

            formData.append('audio', {
                uri: path,
                name: 'audio.aac',
                type: 'audio/aac',
            })
            Toast.show({
                text: "AUDIO RECORD SAVED!",
                buttonText: "Done",
                type: "success",
                position: "top",
                duration: 4000
            })
        }else if(delete_status == true){
            audioData = [['uid',item.uid],['scan_uid',item.scan_uid],['app_url',item.app_url],['audio_list',item.audio_list],['delete_status',delete_status]]
        }
        
        audioData.forEach((detail) => {
            formData.append(detail[0], detail[1]);
        });

        console.log("teste",formData)

        await fetch(`${baseUrl}${audioUrl}`, 
        {
          method: "POST",
          body:formData, // <-- Post parameters        
        })
        .then((response) => response.json())
        .then((responseData) =>{  
        //   console.log("res",responseData)
            Toast.show({
                text: "Operating Success",
                buttonText: "Done",
                type: "success",
                position: "top",
                duration: 4000
            })
          dispatch(saveAudioSuccess(responseData));
        })
        
    } catch (e) {
        Toast.show({
            text: "Operating Failed",
            buttonText: "Done",
            type: "danger",
            position: "top",
            duration: 4000
        })
        dispatch(saveAudioFailed(e));
    }
}

const saveAudioRequsting = () => ({
    type:'SAVE_AUDIO_REQUESTING',
})
const saveAudioSuccess = (user) => ({
    type:'SAVE_AUDIO_SUCCESS',
    user
})
const saveAudioFailed = (errorMessage) => ({
    type:'SAVE_AUDIO_FAILED',
    errorMessage
})