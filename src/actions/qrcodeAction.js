import {scanUser,baseUrl,appUrl} from './../config/constants'
export const getScanUser = (userId,scanId,app_url) => async dispatch => {

    try {
        dispatch(getScanUserRequsting());
        console.log('app_url',app_url)
        await fetch(`${baseUrl}${scanUser}${userId}`, 
        {
          method: "POST",
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
          },
          body:JSON.stringify({
            scan_uid:scanId,
            app_url:app_url
          }) // <-- Post parameters        
        })
        .then((response) => response.json())
        .then((responseData) =>{
          dispatch(getScanUserSuccess([responseData]));
        })
        
    } catch (e) {
        dispatch(getScanUserFailed(e));
    }
}

const getScanUserRequsting = () => ({
    type:'GET_SCAN_USER_REQUESTING',
})
const getScanUserSuccess = (user) => ({
    type:'GET_SCAN_USER_SUCCESS',
    user
})
const getScanUserFailed = (errorMessage) => ({
    type:'GET_SCAN_USER_FAILED',
    errorMessage
})