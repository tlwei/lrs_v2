import {getUrl} from './../config/function'
import {appUrl,baseUrl,loginUser} from './../config/constants'
export const getAppTheme = (baseUrl,getApp) => async dispatch => {
    try {
        dispatch(getAppRequsting());

        await fetch(`${baseUrl}api/getApp/${getApp}`, 
          {
            method: "GET",
            headers: {
              Accept: 'application/json',
            } 
          }) 
          .then((response) => response.json())
          .then(responseData =>{
            console.log("2")
            dispatch(getAppSuccess(responseData));
            console.log("succ",responseData)
          })
    } catch (e) {
        console.log("3")
        dispatch(getAppFailed(e));
    }
}

const getAppRequsting = () => ({
    type:'GET_APP_REQUESTING',
})
const getAppSuccess = (theme) => ({
    type:'GET_APP_SUCCESS',
    theme
})

const getAppFailed = (errorMessage) => ({
    type:'GET_APP_FAILED',
    errorMessage
})