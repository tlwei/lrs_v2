
export const getLang = (lang) => async dispatch => {

//action >> value get from container
  if(lang == "中文"){
      dispatch(_getCh());
  }else{
    dispatch(_getEn());
  }
};

const _getCh = () => ({
  //get function from reducer
  type: 'GET_CH',
});

const _getEn = () => ({
  type: 'GET_EN',
});

