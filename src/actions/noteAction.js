import {noteUrl,baseUrl,appUrl} from './../config/constants'
import { Toast } from 'native-base';
export const saveNote = (item,note,delete_status) => async dispatch => {

    try {
        dispatch(saveNoteRequsting());
        let noteBody = {
            uid:item.uid,
            scan_uid:item.scan_uid,
            app_url:item.app_url,
            scan_name:item.name
        }
        if(note !== undefined){
            noteBody = {
                uid:item.uid,
                scan_uid:item.scan_uid,
                app_url:item.app_url,
                note_list:item.note_list,
                scan_name:item.name,
                note:note,
            }
            console.log('noteBody',noteBody)
            
        }else if(delete_status == true){
            noteBody = {
                uid:item.uid,
                scan_uid:item.scan_uid,
                app_url:item.app_url,
                note_list:item.note_list,
                delete_status:delete_status
            }
        }
        
        await fetch(`${baseUrl}${noteUrl}`, 
        {
          method: "POST",
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
          },
          body:JSON.stringify(noteBody) // <-- Post parameters        
        })
        .then((response) => response.json())
        .then((responseData) =>{
            
            console.log("res",responseData)
            if(note !== undefined){
                if(responseData.message){
                    Toast.show({
                        text: "NOTE FORMAT INVALID!",
                        buttonText: "Done",
                        type: "danger",
                        position: "top",
                        duration: 4000
                    })
                }else{
                    Toast.show({
                        text: "NOTE SAVED!",
                        buttonText: "Done",
                        type: "success",
                        position: "top",
                        duration: 4000
                    })
                }
            }
            
          dispatch(saveNoteSuccess(responseData));
          console.log('responseData',responseData)
        })
        
    } catch (e) {
        dispatch(saveNoteFailed(e));
    }
}

const saveNoteRequsting = () => ({
    type:'SAVE_NOTE_REQUESTING',
})
const saveNoteSuccess = (user) => ({
    type:'SAVE_NOTE_SUCCESS',
    user
})
const saveNoteFailed = (errorMessage) => ({
    type:'SAVE_NOTE_FAILED',
    errorMessage
})