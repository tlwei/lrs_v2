import React, { Component } from 'react';
import { Dimensions,Image } from 'react-native';
import {headerUrl} from './../config/constants'
export default class HeaderImage extends Component {

    render() {
        let header = {uri:this.props.headerPic}
        return (
            // 1000 * 750
            <Image source={header} style={{height: Dimensions.get('window').height/2.7, width: null}}/>
        )
    }
}
