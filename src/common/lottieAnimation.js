import React from 'react';
import { Animated } from 'react-native';
import LottieView from 'lottie-react-native';

export default class LottieIcon extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      onAnimatedLottie:'nope',
      progress: new Animated.Value(0),
    };
  }

  componentDidMount() {
    this.animation.play(this.props.frame[0],this.props.frame[1]);

  }

  componentDidUpdate(){
    if (this.props.tintColor) {
      this.animation.play(this.props.frame[0],this.props.frame[1]);
    }
  }

  render() {
    return (
        <LottieView
          style={this.props.style ? this.props.style : {width: '400%', height: '400%'}}
          name={this.props.name}
          ref={animation => {
          this.animation = animation;
          }}
          source={this.props.tintColor === 'yellow' ?
              this.props.lottieActive //active
              :
              this.props.lottieDefault //default
          } progress={this.state.progress}
          loop={this.props.loop}
          imageAssetsFolder='images'
        />

    );
  }
}

