import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Text } from 'native-base';
import { StyleSheet } from 'react-native';
import { commonFontSize } from '../config/constants'

const mapStateToProps = (state) => {
	return {
		appTheme:state.loginReducer.appThemeList && state.loginReducer.appThemeList
	};
};

class AllText extends Component {
  render() {
    const app = this.props.appTheme
    return (
        <Text note numberOfLines={1} 
         style={
             {
               textAlign: this.props.left == true ? 'left' : 'center',
               color:this.props.color !== undefined ? this.props.color : app && app.title_color,
               fontSize: app && Number(app.font_size)
              }

          }
        >{this.props.text}</Text>
    ); 
  }
}


export default connect(mapStateToProps)(AllText);