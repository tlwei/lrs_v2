import React from 'react';
import { ActivityIndicator, View,Modal, StyleSheet  } from 'react-native';

export default class LoadingScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading:this.props.loading,
    }
   }

  render() {

    return (
        <Modal transparent={true} visible={this.props.loading} onRequestClose={() => {console.log('close modal')}}>
          <View style={styles.modalBackground}>
            <View style={styles.activityIndicatorWrapper}>
              <ActivityIndicator
                animating={this.props.loading} />
            </View>
          </View>
        </Modal>
    );
  }
}
const styles = StyleSheet.create({
modalBackground: {
  flex: 1,
  alignItems: 'center',
  flexDirection: 'column',
  justifyContent: 'space-around',
  backgroundColor: '#00000040'
},
activityIndicatorWrapper: {
  backgroundColor: '#FFFFFF',
  height: 80,
  width: 80,
  borderRadius: 10,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-around'
}
});