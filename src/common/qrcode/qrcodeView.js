import React from "react";
import { Animated, Easing, View,Modal, StyleSheet,Text  } from "react-native";
import LottieView from "lottie-react-native";

export default class Qrcode extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      progress: new Animated.Value(0),
    };
  }

  componentDidMount() {
      console.log("play")
      this.animation.play()
  }

  render() {

    return (
 
        <View style={styles.modalBackground}>
          <LottieView
            style={styles.activityIndicatorWrapper}
            ref={animation => {
            this.animation = animation;
            }}
            source={require("./../../../lottie/QRCODE.json")} progress={this.state.progress}
            loop={true}
            imageAssetsFolder="images"
          />
        </View>

    );
  }
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        position:"absolute",alignSelf: 'center',
        alignItems: "center",
        flexDirection: "column",
        justifyContent: "space-around",
    },
    activityIndicatorWrapper: {
        alignSelf: 'center',
        height: 230,
        width: 230,
        display: "flex",
        alignItems: "center",

    }
});