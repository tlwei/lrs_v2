import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Button, Text, Icon } from 'native-base';
import { StyleSheet } from 'react-native';

const mapStateToProps = (state) => {
	return {
		appTheme:state.loginReducer.appThemeList && state.loginReducer.appThemeList
	};
};
class AllButton extends Component {
  
  render() {
    const app = this.props.appTheme
    return (
        <Button onPress={this.props.navigateTo} style={[styles.button,{backgroundColor:this.props.background ? this.props.background : app && app.btn_color}]}>
          <Text style={{fontSize:16,color:app && app.btn_text}}>{this.props.buttonTitle}</Text>
          {this.props.qrIcon && <Icon name={'md-qr-scanner'}/>}
        </Button>
    );
  }
} 

const styles = StyleSheet.create({
  button:{
      borderRadius:4,
      width: '85%',
      justifyContent: 'center',
      alignSelf:'center',
      marginTop:15
  }
});

export default connect(mapStateToProps)(AllButton);