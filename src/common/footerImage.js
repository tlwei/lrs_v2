import React, { Component } from 'react';
import { Footer, FooterTab } from 'native-base';
import { Image } from 'react-native';
import {baseUrl} from './../config/constants'
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
	return {
		footer_img:state.loginReducer.appThemeList&&state.loginReducer.appThemeList.footer_img        
	};
};

class FooterImage extends Component {
    constructor(props){
		super(props)
	}

    render() {
        let footer = {uri:`${baseUrl}app/${this.props.footer_img && this.props.footer_img}`}
        return (
            <Footer style={{backgroundColor:'#0000'}}>
                <FooterTab>
                    {/* 1000*140 */}
                    {/* require('./../../appImages/footer.jpg') */}
                    {/* <Image source={footer} style={{ height: 55, flex: 1}}/> */}
		            <Image source={require('./../../assets/imgs/footer_lrs.jpeg')} style={{ height: 55, flex: 1}}/>
	            </FooterTab>
            </Footer>
        )
    }
}

export default connect(mapStateToProps)(FooterImage);