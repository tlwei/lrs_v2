import React, { Component } from 'react';
import { Dimensions,Image,TouchableOpacity } from 'react-native';
import {Icon} from 'native-base';
import {headerUrl} from './../config/constants';
export default class HeaderImage extends Component {

    render() {
        let header = {uri:headerUrl}
        return (
            // 1000 * 750
            <TouchableOpacity onPress={this.props.navigationTo}>
				<Icon style={this.props.right == true ?{paddingRight:10}:{paddingLeft:10}} name={this.props.name} size={30}/>
			</TouchableOpacity>
        )
    }
}
