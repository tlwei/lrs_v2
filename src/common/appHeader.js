import React from 'react';
import { Right, Header, Button, Icon,Body,Title } from 'native-base';
import { TouchableOpacity } from 'react-native'


export default class AppHeader extends React.Component {

  render() {
    return (
      <Header style={{ backgroundColor:this.props.backgroundColor || "#2d419c"}}>
        <Button transparent onPress={this.props.navigateTo}>
            <Icon style={{color:this.props.iconColor || 'white'}} name='ios-arrow-back' />
        </Button>
        <Body>
          <Title style={{color:this.props.color || 'white'}}>{this.props.title}</Title>
        </Body>
        <Right />
      </Header>
    );
  }
}
