import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import LoadingScreen from './loading'
function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <LoadingScreen loading={true} />
      <Text style={styles.offlineText}>No Internet Connection</Text>
    </View>
  );
}
class OfflineNotice extends PureComponent {
  render() {
      return <MiniOfflineSign />;
  }
}
const styles = StyleSheet.create({
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',


  },
  offlineText: {
    color: '#fff'
    
  }
});
export default OfflineNotice;
