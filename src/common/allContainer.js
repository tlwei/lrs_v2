import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Container} from 'native-base';

const mapStateToProps = (state) => {
	return {
		appTheme:state.loginReducer.appThemeList && state.loginReducer.appThemeList
	};
};
class AllContainer extends Component {
  
  render() {
    const app = this.props.appTheme

    return (
        <Container style={{backgroundColor:app && app.background_color}}>	
            {this.props.content}
        </Container>
    );
  }
} 

export default connect(mapStateToProps)(AllContainer);