import React, { Component } from 'react';
import { Icon,Right,Left, ListItem, Text } from 'native-base';
import AllText from './allText'; 
import {connect} from 'react-redux';


const mapStateToProps = (state) => {
	return {
		appTheme:state.loginReducer.appThemeList && state.loginReducer.appThemeList
	};
};

class ScanList extends Component {
  render() {
    const app = this.props.appTheme
    return (
		  <ListItem style={{backgroundColor:app && app.background_color,marginLeft: 0,paddingLeft: 17}} button onPress={this.props.navigateTo}>
          <Left>
            <AllText text={this.props.player}/>
          </Left>
          <Right>
            <Icon style={{color:app && app.title_color}} name={app && app.list_icon} />
          </Right>
      </ListItem>
    );
  }
}

export default connect(mapStateToProps)(ScanList);