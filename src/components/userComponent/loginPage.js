import React, { Component } from 'react';
import { Container,Item,Toast,Label,Input, Header, Content, Footer, FooterTab, Button, Icon, Text, Form } from 'native-base';
import { TouchableOpacity,NativeModules,AsyncStorage,Dimensions,Alert,Image,TextInput,StyleSheet,View } from 'react-native';
import HeaderImage from './../../common/headerImage'
import FooterImage from './../../common/footerImage'
import LoadingScreen from './../../common/loading'
import AllButton from './../../common/allButton'
import {baseUrl,getApp} from './../../config/constants'
import DeviceInfo from 'react-native-device-info';

class loginPage extends Component {
	constructor(props){
		super(props)
		this.state={
			user:'',
			password:'',
			deviceID:DeviceInfo.getUniqueID(),
			deviceName:DeviceInfo.getDeviceName()
		}
	}

	componentDidMount(){
		AsyncStorage.getItem('lang').then((lang)=>{
			if(lang){
				this.props.getLang(lang)
			}else{
				this.props.getLang('en')
			}
			
			})
		this.props.logoutUser()
		AsyncStorage.getItem('user').then((user)=>{
			this.setState({user:user})
		})
		AsyncStorage.getItem('password').then((password)=>{
			this.setState({password:password})
		})
		
		// this.props.getApp(baseUrl,getApp)
	}
   
	componentWillReceiveProps(nextProps){
		if(nextProps.user && nextProps.user[0][0].app_url){
			this.props.getApp(baseUrl,nextProps.user[0][0].app_url)
		}
		if(this.props.navigation.state.params){
			AsyncStorage.setItem('user','')
			AsyncStorage.setItem('password','')
		}
		if(nextProps.user !== undefined){
			if(typeof nextProps.user[0][0] === 'string'){
				if(nextProps.user[0][0] !== Array){
					Alert.alert('Login Tips',nextProps.user[0][0],[{text: 'OK', onPress: () => console.log('OK Pressed')}]);
				}
			}else{
				console.log("saved!")
				Toast.show({text: "USER LOGGED IN!",buttonText: "Done",type: "success",position: "top",duration: 1000})
				AsyncStorage.setItem('user',this.state.user)
				AsyncStorage.setItem('password',this.state.password)
				nextProps.navigation.navigate('WelcomeScreen')
			}
		}
	}

	submitLoginForm(){
		this.props.getUser(this.state.user,this.state.password,false,this.state.deviceID,this.state.deviceName)
		// this.props.getApp(baseUrl,getApp)
	}

  render() {

	const lang = this.props.lang && this.props.lang.Login
    return (

      <Container style={[styles.container,{backgroundColor:'rgb(241, 243, 244)'}]}>
		<Content>
        {/* <LoadingScreen loading={this.props.fetching} /> */}
			<LoadingScreen loading={this.props.loginFetch} />
			{/* 1000 * 750 */}
			<Image source={require('./../../../assets/imgs/app_header.jpg')} style={{height: Dimensions.get('window').height/2.7, width: null}}/>
				{/* <HeaderImage headerPic={`${baseUrl}app/${app && app.heading_img}`} /> */}
				
        <View>
          <Text style={[styles.welcome,{color:'rgb(73, 73, 73)'}]}>SEI Event Retrieval App</Text>
        </View>
					
			<Form>
				<Item>
					<Input style={{height:55,color:'rgb(73, 73, 73)'}}
					placeholder={lang[0]}
        			onChangeText={
						 value => 
						 	{
								this.setState({ user: value })
								AsyncStorage.setItem('user','')	
							}
					} value={this.state.user}/>
        		</Item>
				<Item>
					<Input style={{height:55,color:'rgb(73, 73, 73)'}}
					 placeholder={lang[1]}
					 secureTextEntry={true}
        	 		 onChangeText={
						value => 
						{
							this.setState({ password: value })
							AsyncStorage.setItem('password','')
						}
					} 
					onSubmitEditing={()=>this.submitLoginForm()} value={this.state.password}/>
        		</Item>
			</Form>
			<Button onPress={()=>this.submitLoginForm()} style={[styles.button,{backgroundColor:'steelblue'}]}>
        <Text style={{fontSize:16,color:'white'}}>{lang[2]}</Text>
      </Button>
 
			<Button onPress={()=>
				{
					AsyncStorage.setItem('user','')
					AsyncStorage.setItem('password','')
					this.props.navigation.navigate('Qrcode',{status:'login'})
				}} style={[styles.button,{backgroundColor:'steelblue'}]}>
        <Text style={{fontSize:16,color:'white'}}>{lang[3]}</Text>
        <Icon name={'md-qr-scanner'}/>
      </Button>
		
			<Button onPress={()=>this.props.navigation.navigate('Language')} style={[styles.button,{backgroundColor:'steelblue'}]}>
        <Text style={{fontSize:16,color:'white'}}>{lang[4]}</Text>
      </Button>

			<Text/><Text/>
			{/* <Text style={{paddingLeft:10,fontSize:8}}>Device ID: {this.state.deviceID}</Text> */}
			{/* <Text style={{paddingLeft:10,fontSize:8}}>Device Name: {this.state.deviceName}</Text> */}
		</Content>
		 {/* 1000 * 140 */}
				<Footer style={{backgroundColor:'#0000'}}>
            <FooterTab>
		        <Image source={require('./../../../assets/imgs/footer_lrs.jpeg')} style={{ height: 55, flex: 1}}/>
	        </FooterTab>
        </Footer> 
      </Container>
    );
  }
}

const styles = StyleSheet.create({
	container: {
	  flex: 1,
	},
	welcome: {
		paddingTop:15,
		fontSize: 24,
		fontWeight:'bold',
	  textAlign: 'center',
	},
	button:{
		borderRadius:4,
		width: '85%',
		justifyContent: 'center',
		alignSelf:'center',
		marginTop:15
}
});

export default loginPage;