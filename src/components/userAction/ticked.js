import React, { Component } from 'react';
import { Content, ListItem, CheckBox, Text, Body } from 'native-base';
import AllContainer from './../../common/allContainer'
import AllText from '../../common/allText';
import AllButton from './../../common/allButton';

export default class Ticked extends Component {
  constructor(props){
    super(props)
    this.state={

    }
  }  

  checked(actionData,status){
    let actionStatus = status == 1 ? 0 : 1;
    this.props.saveAction(actionData,actionStatus)
  }


  render() { 
    console.log('lang',this.props.lang)
		const lang = this.props.lang && this.props.lang.Home
    const app = this.props.appTheme
    let tickedAction = this.props.tickedData || this.props.navigation.state.params.ticked
    if(tickedAction == undefined){
      tickedAction = []
    }

    const content = 
      <Content>
        {tickedAction.map((detail,index)=>(
          <ListItem style={{backgroundColor:app && app.background_color,marginLeft: 0}} key={index} button onPress={()=>{this.checked(detail,detail.status)}}>
            <Body>
              <AllText left={true} text={detail.action} />
            </Body>
            <CheckBox onPress={()=>{this.checked(detail,detail.status)}} checked={this.props.tickFetching == true ? null :detail.status == 1 ? true : false} color="green"/>
          </ListItem>
        ))}
      </Content>
    return (
      <AllContainer content={content} />
    );
  }
}
