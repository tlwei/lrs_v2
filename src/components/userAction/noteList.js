import React, {Component} from 'react';
import { Platform,Dimensions,FlatList,ListView,StyleSheet,Alert,Text,Slider,TouchableOpacity,TouchableHighlight,View} from 'react-native';
import AllText from '../../common/allText';
import ScanList from '../../common/scanList'
import { Container,Icon,Button,Right,Left,Body, Header, Content, List, ListItem, Footer, FooterTab } from 'native-base';
import {userData,appUrl, listIcon} from './../../config/constants';
import AllContainer from '../../common/allContainer';
import LoadingScreen from './../../common/loading'

class App extends Component {
  
	////row
	constructor(props) {
		super(props);
		this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.state = {
			audioBar:false,
			listViewData: this.props.noteData,
			playState:'paused', //playing, paused
            playSeconds:0,
            duration:0,
            status:0,
			audioName:'',
		};
		this.sliderEditing = false;
	}

	closeRow(rowMap, rowKey) {
		if (rowMap[rowKey]) {
			rowMap[rowKey].closeRow();
		}
	} 

	componentWillReceiveProps(nextProps){
		this.setState({listViewData:nextProps.noteData})
	}

	deleteRow(deleteValue) {
		const lang = this.props.lang && this.props.lang.Tips
		console.log('delete',deleteValue)
		Alert.alert(
			lang[0],
			lang[1],
		   [
		     {text: lang[2], onPress: () => console.log('NO Pressed')},
		     {text: lang[3], onPress: () => {
					this.props.saveNote(deleteValue,undefined,true)
		     }},
		   ]
		 );
		// this.props.saveNote(this.state.userData,this.state.text)
		// this.closeRow(rowMap, rowKey);
		// const newData = [...this.state.listViewData];
		// const prevIndex = this.state.listViewData.findIndex(item => item.text === rowKey);
		// newData.splice(prevIndex, 1);
		// this.setState({listViewData: newData});
	}

	onRowDidOpen = (rowKey, rowMap) => {
		console.log('This row opened', rowKey);
		setTimeout(() => {
			this.closeRow(rowMap, rowKey);
		}, 2000);
	}

	render() {
		const lang = this.props.lang && this.props.lang.Notes
		let note = this.state.listViewData ? this.state.listViewData.length : 0;
		let userData = this.props.navigation.state.params && this.props.navigation.state.params;

		const content = 
		  <Content>
			<LoadingScreen loading={this.props.noteFetching} />
		  	<List> 
		  		<ListItem itemDivider button onPress={()=>this.props.navigation.navigate('NotesData',{
							 note:
							 	{
								 item:
								 {
									uid: userData && userData.uid,
									scan_uid: userData && userData.scan_uid,
									app_url: userData && userData.app_url,
									name:userData && userData.name,
									note_list:note+1,
									note:''
								 }
								}
							})}>
		  			<Left>
		  				<AllText color={'rgb(81, 81, 81)'} text={`${lang[1]}: `+note}/>
		  			</Left>
		  		   <Right>
		  					 <Icon name="md-add-circle" style={{fontSize:35}} />
		  		   </Right>
		  		</ListItem>  

					<FlatList 
              data={this.state.listViewData}
							renderItem={ (data, rowMap) =>
								<ListItem button onPress={()=>this.props.navigation.navigate('NotesData',{note:data})}>
									<Left>
										<AllText text={data.item.note}/>
									</Left>
									<Right>
									<TouchableOpacity onPress={ _ => this.deleteRow(data.item) }>
										<Icon name="trash" style={{fontSize:35,color:'black'}} />
									</TouchableOpacity>

									</Right>
								</ListItem>
          		}
              keyExtractor={(item,index) => index.toString()}
          />

		  	</List>
		  </Content>
		return (
		    <AllContainer content={content} />
		);
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'white',
		flex: 1
	},
	backTextDel: {
		color: '#FFF',
        fontWeight:'bold'
	},
    backTextEdit: {
        color: '#FFF',
        fontWeight:'bold'
	},
	rowFront: {
		backgroundColor: 'white',
		justifyContent: 'center',
        
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: 'white',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 25,
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 75
	},
	backRightBtnLeft: {
		backgroundColor: 'rgb(65, 133, 244)',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: 'rgb(234, 67, 52)',
		right: 0
	},
});

export default App;