import React from "react";
import { Platform,ActivityIndicator,TouchableOpacity,FlatList,Animated, Easing, View,Modal, StyleSheet,NativeModules  } from "react-native";
import { Separator,List, ListItem,Container, Button,Content, Text,Title,Thumbnail, Right,Left,CardItem ,Icon,Body,Header } from "native-base";
import AppHeader from "../../common/appHeader";
import AllText from "../../common/allText";
import codePush from 'react-native-code-push';

export default class HomeIcon extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      index: 0,
      default:[
        ['English','Default Language','English'],
        ['Chinese','默认语言','中文'],
      ],
    }
  }

  render() {
    const lang = this.props.lang && this.props.lang.Login
    return (
        <Container>
            <AppHeader backgroundColor={'rgb(193, 194, 195)'} navigateTo={() => this.props.navigation.navigate('LoginScreen')} title={lang[5]} />
            <FlatList 
              data={this.state.default}
              keyExtractor={(index) => index.toString()}
              renderItem={({item,index}) => 
              <ListItem button onPress={() => {
                this.props.getLang(item[2])
              
                setTimeout(() => {
                  codePush.restartApp(); 	
                }, 500);
                
              }} icon>
                  <Body> 
                    <AllText text={item[2]}/>
                  </Body>
                  
              </ListItem>
            }/>
            
        </Container>

    );
  }
}
const styles = StyleSheet.create({
modalBackground: {
  flex: 1,
  alignItems: 'center',
  flexDirection: 'column',
  justifyContent: 'space-around',
  backgroundColor: '#00000040'
},
activityIndicatorWrapper: {
  backgroundColor: '#FFFFFF',
  height: 80,
  width: 80,
  borderRadius: 10,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-around'
}
});