'use strict';
 
import React, { Component } from 'react';
import {AppRegistry,AsyncStorage,StyleSheet,Linking,Alert} from 'react-native';
import { Container,Form,Item,Label,Input, Header, Title, Content, Button, Icon, Card, CardItem, Text, Body, Left, Right, IconNB, View, Toast, FooterTab, Footer } from "native-base";
import QRCodeScanner from './../../common/qrcode/qrcodeFunc';
import AllButton from './../../common/allButton';
import AppHeader from './../../common/appHeader'
import FooterImage from './../../common/footerImage'
import DeviceInfo from 'react-native-device-info';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class ScanScreen extends Component {
  constructor(props){
		super(props)
		this.state={
      inputId:'',
      user:'',
      password:'',
      deviceID:DeviceInfo.getUniqueID(),
			deviceName:DeviceInfo.getDeviceName()
		}
  }
  
  static navigationOptions = {
    header: null,
  };

  onSuccess(e) {
    const lang = this.props.lang && this.props.lang.QrTips
    let status = this.props.navigation.state.params.status;
    // console.log('qrlogin success',e.data)
    let data = e.data;
    let checkLogin = data.search('&UserQrcodeLogin=');
    let checkScan = data.length;
    let app_url = this.props.user && this.props.user[0][0].app_url
    // Toast.show({
          //   text: checkScan,
          //   buttonText: "Done",
          //   type: "danger",
          //   position: "top",
          //   duration: 6000 
          // })
    if(checkLogin !== -1 && status == 'login'){
      
      let string = data.replace("&UserQrcodeLogin=", ",");
      // let array = JSON.parse("[" + string + "]");
      var array = string.split(",");
      let user = array[0]
      let password = array[1]

      this.props.getUser(user,password,true,this.state.deviceID,this.state.deviceName)  
      
      AsyncStorage.setItem('user',String(user))
			AsyncStorage.setItem('password',String(password))
    }else if(status == 'scan'){
      let userId = this.props.user && this.props.user[0][0].uid
      this.props.getUser('','',true,this.state.deviceID,this.state.deviceName)  
      this.props.getScanUser(userId,data,app_url)
    }else if(status == 'login'){
 
      Alert.alert(lang[0],lang[1],[{text: lang[4], onPress: () => this.props.navigation.navigate('LoginScreen')}]);
    }else{
    
      Alert.alert(lang[0],lang[1],[{text: lang[4], onPress: () => this.props.navigation.navigate('WelcomeScreen')}]);
    }
  } 
  
  componentWillReceiveProps(nextProps){
    //qr alert
    const lang = this.props.lang && this.props.lang.QrTips
    let status = this.props.navigation.state.params.status;

    if(nextProps.user !== undefined && status == 'login'){

      if(typeof nextProps.user[0][0] === 'string'){

        Alert.alert(lang[0],nextProps.user[0][0],[{text: lang[4], onPress: () => this.props.navigation.navigate('LoginScreen')}]);
      }else{

        Alert.alert(
          lang[0],
          lang[6],
          [
            {text: lang[4], onPress: () => {this.props.navigation.navigate('WelcomeScreen')}},
          ],{cancelable: false},
        );
      }
    }
    
    if(nextProps.scanUser !== undefined && status == 'scan'){
			if(typeof nextProps.scanUser[0][0] === 'string'){
					Alert.alert(lang[0],nextProps.scanUser[0][0],[{text: lang[4], onPress: () => console.log('OK Pressed')}]);
			}else{
        Alert.alert(
          lang[0],
          `${lang[2]} ${lang[3]}`,
          [
            {text: lang[5], onPress: () => {this.props.navigation.navigate('WelcomeScreen')}},
          ],{cancelable: false},
        );
			}
		}
	}

  scanUser(userId,scanId){
    let app_url = this.props.user && this.props.user[0][0].app_url
    this.props.getUser('','',true,this.state.deviceID,this.state.deviceName)
    //get scanned
    this.props.getScanUser(userId,scanId,app_url)
  }

  render() {
    
		const lang = this.props.lang && this.props.lang.Qrcode
    const app = this.props.appTheme
    let userId = this.props.user && this.props.user[0][0].uid
    let navigation = this.props.navigation.state.params.status == 'login' ? 'LoginScreen' : 'WelcomeScreen'
    
    return (
      <KeyboardAwareScrollView>
        <Container> 
          

          <AppHeader backgroundColor={'black'} navigateTo={() => this.props.navigation.navigate(navigation)} title={lang[0]} />
          <QRCodeScanner
            style={{backgroundColor:"#00F0F880"}}
            onRead={this.onSuccess.bind(this)}
            topContent={
              <Text style={styles.centerText}>
                {lang[0]}
              </Text>
            }
          />
          
  {/* <View>
    <TextInput />
  </View> */}


          {this.props.navigation.state.params.status == 'scan' &&
          
            <Form style={{backgroundColor:'white',padding:10}}>
             <Item>
                 <Input style={{height:55,color:'black'}}
                  placeholder={lang[2]} onChangeText={value => this.setState({ inputId: value })} value={this.state.inputId}/>
             </Item>
             <AllButton navigateTo={()=>this.scanUser(userId,this.state.inputId)} buttonTitle={lang[3]} />
           </Form>
           
          }
         {this.props.navigation.state.params.status == 'scan' ? <FooterImage /> :<Footer><FooterTab style={{backgroundColor:"black"}}></FooterTab></Footer>}
        
      </Container>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});

AppRegistry.registerComponent('default', () => ScanScreen);