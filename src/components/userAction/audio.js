import React, { Component } from 'react';
import { Image,Dimensions } from 'react-native';
import AllButton from './../../common/allButton';
import AllText from './../../common/allText';
import {micIcon} from './../../config/constants';
import {Text,TouchableOpacity,TouchableHighlight,Platform,PermissionsAndroid} from 'react-native';
import { Content, Container, Toast, Button } from 'native-base';
var Sound = require('react-native-sound')
import {AudioRecorder, AudioUtils} from 'react-native-audio';
import {appUrl} from './../../config/constants'

class Audio extends Component {
      state = {
      currentTime: 0,
      color:null,
      recording: false,
      paused: false,
      status:0,
      stoppedRecording: false,
      finished: false,
      save:false,
      recordText:this.props.lang && this.props.lang.Audio[1],
      audioPath: AudioUtils.DocumentDirectoryPath + '/test.aac',
      hasPermission: undefined,
    };

    prepareRecordingPath(audioPath){
      AudioRecorder.prepareRecordingAtPath(audioPath, {
        SampleRate: 22050,
        Channels: 1,
        AudioQuality: "Low",
        AudioEncoding: "aac",
        AudioEncodingBitRate: 32000
      });
    }

    componentDidMount() {

      AudioRecorder.requestAuthorization().then((isAuthorised) => {
        this.setState({ hasPermission: isAuthorised });

        if (!isAuthorised) return;

        this.prepareRecordingPath(this.state.audioPath);

        AudioRecorder.onProgress = (data) => {
          this.setState({currentTime: Math.floor(data.currentTime)});
        //   Toast.show({
        //     text:this.state.currentTime,
        //     buttonText: "Done",
        //     type: "success",
        //     position: "top",
        //     duration: 4000
        // })
          console.log('time',this.state.currentTime)
          // if(this.state.currentTime <1){
          //   console.log('stop')
          //   this.recordSound(this.state.status+1)
          // }
        };

        AudioRecorder.onFinished = (data) => {
          // Android callback comes in the form of a promise instead.
          if (Platform.OS === 'ios') {
            this._finishRecording(data.status === "OK", data.audioFileURL, data.audioFileSize);
          }
        };
      });
    }

    _renderButton(title, onPress, active) {
      var style = (active) ? styles.activeButtonText : styles.buttonText;

      return (
        <TouchableHighlight style={styles.button} onPress={onPress}>
          <Text style={style}>
            {title}
          </Text>
        </TouchableHighlight>
      );
    }

    _renderPauseButton(onPress, active) {
      var style = (active) ? styles.activeButtonText : styles.buttonText;
      var title = this.state.paused ? "RESUME" : "PAUSE";
      return (
        <TouchableHighlight style={styles.button} onPress={onPress}>
          <Text style={style}>
            {title}
          </Text>
        </TouchableHighlight>
      );
    }

    async _pause() {
      if (!this.state.recording) {
        console.warn('Can\'t pause, not recording!');
        return;
      }

      try {
        const filePath = await AudioRecorder.pauseRecording();
        this.setState({paused: true});
      } catch (error) {
        console.error(error);
      }
    }

    async _resume() {
      if (!this.state.paused) {
        console.warn('Can\'t resume, not paused!');
        return;
      }

      try {
        await AudioRecorder.resumeRecording();
        this.setState({paused: false});
      } catch (error) {
        console.error(error);
      }
    }

    async _stop() {
      if (!this.state.recording) {
        console.warn('Can\'t stop, not recording!');
        Toast.show({
          text: "VOICE RECORD FAILED!",
          buttonText: "Done",
          type: "danger",
          position: "top",
          duration: 6000
        })
        return;
      }

      this.setState({stoppedRecording: true, recording: false, paused: false});

      try {
        const filePath = await AudioRecorder.stopRecording();

        if (Platform.OS === 'android') {
          this._finishRecording(true, filePath);
           
        }
        Toast.show({
          text: "VOICE RECORDED, PLEASE SAVE.",
          buttonText: "Done",
          type: "success",
          position: "top",
          duration: 4000
        })
        return filePath;
      } catch (error) {
        console.error(error);
      }
    }

    async _play() {
      if (this.state.recording) {
        await this._stop();
      }

      // These timeouts are a hacky workaround for some issues with react-native-sound.
      // See https://github.com/zmxv/react-native-sound/issues/89.
      
      const sound = new Sound(
        this.state.audioPath,
        undefined,
        (error) => {
          if (error) {
            console.log(error);
          } else {
            console.log('Playing sound');
            sound.play(() => {
              sound.release();
            });
          }
        }
      );
    }

    async _record() {
      if (this.state.recording) {
        console.warn('Already recording!');
        return;
      }

      if (!this.state.hasPermission) {
        console.warn('Can\'t record, no permission granted!');
        return;
      }

      if(this.state.stoppedRecording){
        this.prepareRecordingPath(this.state.audioPath);
      }

      this.setState({recording: true, paused: false});

      try {
        const filePath = await AudioRecorder.startRecording();
      } catch (error) {
        console.log('err',error);
      }
    }

    _finishRecording(didSucceed, filePath, fileSize) {
      this.setState({ finished: didSucceed });
      console.log(`Finished recording of duration ${this.state.currentTime} seconds at path: ${filePath} and size of ${fileSize || 0} bytes`);
    }

/////
	recordSound(value){
    const lang = this.props.lang && this.props.lang.Audio
    if(value == 1){
      console.log("record",value)
      this._record();
      Toast.show({
        text: lang[8],
        buttonText: "Done",
        position: "top",
        duration: 15000 
      }) 
      this.setState({color:'red',status:value,recordText:lang[2]})
    }else{
      
      // this.saveAudio();
      this._stop();
      this.setState({status:0,save:true,recordText:lang[3]})
      setTimeout(() => {
        this.setState({color:null,recordText:lang[1],currentTime:0})
      }, 1600);
    
    }
  } 

  saveAudio=()=>{
    if(this.state.save == true){
      let userData = this.props.navigation.state.params
      let audioLength = this.props.audioData ? this.props.audioData.length : 0
      let item = {
        uid : userData.uid,
        scan_uid : userData.scan_uid,
        app_url : userData.app_url,
        audio_list : audioLength+1,
        name : `audio_${audioLength+1}`
      }

      this.props.saveAudio(item,this.state.audioPath)
    }else{
      Toast.show({
          text:"Please Record Voice First.",
          buttonText: "DONE",
          type: "danger",
          position: "top",
          duration: 4000
      })
    }
  }

  render() {
		const lang = this.props.lang && this.props.lang.Audio
//audio sound less thant 10 s
		let mic = {uri:micIcon};
    return ( 
			<Container>	
         {/* <AllButton navigateTo={()=>this._play()} buttonTitle={'SOUNDEM'} /> */}
          <Content style={{marginTop:40}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('AudioList')}>
              <Image source={mic} style={{ height:Dimensions.get('window').height/2,width: null}}/>
            </TouchableOpacity>
            <AllText text={`${this.state.currentTime}${lang[5]}`}/>
            <AllButton background={this.state.color} navigateTo={()=>this.recordSound(this.state.status+1)} buttonTitle={this.state.recordText} />
            <AllButton navigateTo={()=>this.saveAudio()} buttonTitle={lang[4]} />
          </Content>

			</Container>
    );
  }
}

export default Audio;

// start ==> this._record();
// stop ==> this._stop();
// play ==> this._play();

