import React, {Component} from 'react';
import { Platform,Alert,Dimensions,ListView,FlatList,StyleSheet,Text,Slider,TouchableOpacity,TouchableHighlight,View} from 'react-native';
var Sound = require('react-native-sound')
import AllText from '../../common/allText';
import ScanList from '../../common/scanList'
import { Container,Icon,Button,Right,Left,Body, Header, Content, List, ListItem, Footer, FooterTab, Toast } from 'native-base';
import {userData,baseUrl} from './../../config/constants';
import LoadingScreen from './../../common/loading'

class App extends Component {
	constructor(){
        super();
        this.state = {

			audioBar:false,
			listViewData: userData[0].audio,
			playState:'paused', //playing, paused
            playSeconds:0,
            duration:0,
            status:0,
			icon:'md-play',
			audioName:'',
			audioURL:''
        }
        this.sliderEditing = false;
    }

    componentDidMount(){
        
        this.timeout = setInterval(() => {
            if(this.sound && this.sound.isLoaded() && this.state.playState == 'playing' && !this.sliderEditing){
                this.sound.getCurrentTime((seconds, isPlaying) => {
                    this.setState({playSeconds:seconds});
                })
            }
        }, 100);
	}
	

    componentWillUnmount(){
        if(this.sound){
            this.sound.release();
            this.sound = null;
        }
        if(this.timeout){
            clearInterval(this.timeout);
        }
	}
	
	deleteRow(deleteValue) {
		const lang = this.props.lang && this.props.lang.Tips
		Alert.alert(
			lang[0],
			lang[1],
		   [
		     {text: lang[2], onPress: () => console.log('NO Pressed')},
		     {text: lang[3], onPress: () => {
				 this.props.saveAudio(deleteValue,undefined,true)
		     }},
		   ]
		 );
		// this.props.saveNote(this.state.userData,this.state.text)
		// this.closeRow(rowMap, rowKey);
		// const newData = [...this.state.listViewData];
		// const prevIndex = this.state.listViewData.findIndex(item => item.text === rowKey);
		// newData.splice(prevIndex, 1);
		// this.setState({listViewData: newData});
	}

    onSliderEditStart = () => { 
        this.sliderEditing = true;
    }
    onSliderEditEnd = () => {
        this.sliderEditing = false;
    }
    onSliderEditing = value => {
        if(this.sound){
            this.sound.setCurrentTime(value);
            this.setState({playSeconds:value});
        }
    }

    play = async (soundUrl) => {
		this.setState({status:2})
	
        if(this.sound && this.sound._filename == soundUrl){
            this.sound.play(this.playComplete);
            this.setState({status:1,playState:'playing'});
        }else{
            this.sound = new Sound(`${baseUrl}app${soundUrl}`, '', (error) => {
				this.setState({status:1})
                if (error) {
					this.setState({playState:'paused'});
					Toast.show({
						text: "AUDIO PLAYBACK FAILED.",
						buttonText: "Done",
						position: "top",
						type: "danger",
						duration: 3000
					})
                }else{
                    this.setState({playState:'playing', duration:this.sound.getDuration()});
                    this.sound.play(this.playComplete);
                }
            });    
        }
    }
    playComplete = (success) => {
        if(this.sound){
            if (success) {
				this.audioPlay(this.state.status,this.state.audioUrl,this.state.audioName)
                console.log('successfully finished playing');
            } else {
                console.log('playback failed due to audio decoding errors');
                Alert.alert('Notice', 'audio file error. (Error code : 2)');
            }
            this.setState({playState:'paused', playSeconds:0});
            this.sound.setCurrentTime(0);
        }
    }

    pause = () => {
        if(this.sound){
            this.sound.pause();
        }

        this.setState({playState:'paused'});
    }

    jumpPrev15Seconds = () => {this.jumpSeconds(-15);}
    jumpNext15Seconds = () => {this.jumpSeconds(15);}
    jumpSeconds = (secsDelta) => {
        if(this.sound){
            this.sound.getCurrentTime((secs, isPlaying) => {
                let nextSecs = secs + secsDelta;
                if(nextSecs < 0) nextSecs = 0;
                else if(nextSecs > this.state.duration) nextSecs = this.state.duration;
                this.sound.setCurrentTime(nextSecs);
                this.setState({playSeconds:nextSecs});
            })
        }
    }

    getAudioTimeString(seconds){
        const h = parseInt(seconds/(60*60));
        const m = parseInt(seconds%(60*60)/60);
        const s = parseInt(seconds%60);

        return ((h<10?'0'+h:h) + ':' + (m<10?'0'+m:m) + ':' + (s<10?'0'+s:s));
    }
    ///audio play
    audioPlay(status,audioUrl,audioName){
	  let url = audioUrl;
      if(status == 0){
        this.play(url);
        this.setState({icon:'md-pause'})
      }else if(status == 1){
        this.pause();
        this.setState({status:0,icon:'md-play'})
	  }else{
		Toast.show({
			text: "AUDIO ON LOADING",
			buttonText: "Done",
			position: "top",
			duration: 2000
		})
	  }
	}

	render() {
		const lang = this.props.lang && this.props.lang.Audio
		const app = this.props.appTheme
		const currentTimeString = this.getAudioTimeString(this.state.playSeconds);
		const durationString = this.getAudioTimeString(this.state.duration);
		let audioLength = this.props.audioData ? this.props.audioData.length : 0
		return (
		    <Container style={{backgroundColor:app && app.background_color}}>
			<LoadingScreen loading={this.props.audioFetching} />
                <Content>
                    <List> 
                        <ListItem itemDivider>
                            <AllText color={'rgb(81, 81, 81)'} text={`${lang[7]}: ` + audioLength }/>
                        </ListItem>  

						<FlatList 
          				    data={this.props.audioData}
								renderItem={ (data, rowMap) =>
									<ListItem button onPress={()=>{
										this.setState({audioUrl:data.item.audio,audioName:data.item.name,audioBar:true})
										this.audioPlay(this.state.status,data.item.audio,data.item.name)
									}}> 
										<Left>
											<AllText text={data.item.name}/>
										</Left>
										<Right>
										<TouchableOpacity onPress={ _ => this.deleteRow(data.item) }>
											<Icon name="trash" style={{fontSize:35,color:'black'}} />
										</TouchableOpacity>
							
										</Right>
									</ListItem>
          						}
          				    keyExtractor={(item,index) => index.toString()}
          				/>
			        </List>
                </Content>
				{this.state.audioBar == true && 
                	<Footer>
          			  <FooterTab>
          			  	<View style={{flex:1, justifyContent:'center', backgroundColor:'#c0c0c0'}}>	
          			  	    <View style={{marginVertical:15, marginHorizontal:15, flexDirection:'row'}}>
          			  	    <TouchableOpacity 
          			  	      onPress={()=>this.audioPlay(this.state.status,this.state.audioUrl,this.state.audioName)} style={{padding:5}}><Icon style={{color:'white'}} name={this.state.icon}/></TouchableOpacity>
          			  	        <Text> </Text>
          			  	        <Text style={{color:'white', alignSelf:'center'}}>{currentTimeString}</Text>
          			  	        <Slider
          			  	            onTouchStart={this.onSliderEditStart}
          			  	            onTouchEnd={this.onSliderEditEnd}
          			  	            onValueChange={this.onSliderEditing}
          			  	            value={this.state.playSeconds} maximumValue={this.state.duration} maximumTrackTintColor='gray' minimumTrackTintColor='white' thumbTintColor='white' 
          			  	            style={{flex:1, alignSelf:'center', marginHorizontal:Platform.select({ios:5})}}/>
          			  	        <Text style={{color:'white', alignSelf:'center'}}>{durationString}</Text>
          			  	    </View>
          			  	</View>
          			  </FooterTab>
          			</Footer>
				}
            </Container>
		);
	}
}
// 
// 

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'white',
		flex: 1
	},
	backTextDel: {
		color: '#FFF',
        fontWeight:'bold'
	},
    backTextEdit: {
        color: '#FFF',
        fontWeight:'bold'
	},
	rowFront: {
		backgroundColor: 'white',
		justifyContent: 'center',
        
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: 'white',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 25,
	},
	backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 75
	},
	backRightBtnLeft: {
		backgroundColor: 'rgb(65, 133, 244)',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: 'rgb(234, 67, 52)',
		right: 0
	},
});

export default App;
