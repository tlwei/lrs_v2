import React, { Component } from 'react';
import { Container, Header, Content, Textarea, Form, View, Footer, FooterTab } from 'native-base';
import {StyleSheet,Dimensions,Text,Platform} from 'react-native';
import AllButton from './../../common/allButton';
import AllContainer from '../../common/allContainer';
export default class TextArea extends Component {
  constructor(props){
    super(props)
    this.state={
      text:this.props.navigation.state.params.note.item.note,
      userData:this.props.navigation.state.params.note.item,
    }
  }
  saveNote(){
    this.props.saveNote(this.state.userData,this.state.text) 
  }

  componentWillReceiveProps(nextProps){
    nextProps.saveNote(this.state.userData,this.state.text)
  }

  render() {
		const lang = this.props.lang && this.props.lang.Notes
    const app = this.props.appTheme
    const content =
    <Container>
      <Content padder> 
        <Form>
          <Textarea style={[styles.textStyle,{color:'rgb(81, 81, 81)'}]} 
           rowSpan={10} 
           bordered 
           placeholder={lang[2]}
           onChangeText={(value) => {
             this.setState({text:value});
           }}
           value={this.state.text}
           />
        </Form>
         
        </Content>
        <Container style={{flex:Platform.OS == 'ios' ? 1 : 0.5}}>

            <AllButton navigateTo={()=>this.saveNote()} buttonTitle={lang[3]} />

        </Container>
      </Container>
    return (
      <AllContainer content={content} />
    );
  }
}

const styles=StyleSheet.create({
  textStyle:{
    height:Dimensions.get('window').height/1.8,
    fontSize: 20,
    borderRadius:10,
    backgroundColor:'white',
	  color:'rgb(81, 81, 81)',
  }
})