import React, { Component } from 'react';
import {StyleSheet} from 'react-native';
import { Container,View,Body,Card,CardItem,Button,Header, Content, Thumbnail, Text } from 'native-base';
import AllText from './../../common/allText';
import AllButton from './../../common/allButton';
import AllContainer from './../../common/allContainer';
import {baseUrl} from './../../config/constants';
class HotLeads extends Component {
	constructor(props){
		super(props)
		this.state={

		}
	}

	userAction(action){
		
		let actionData = this.props.navigation.state.params.profile;

		if(action == 'ticked'){
			this.props.saveAction(actionData,undefined)
			this.props.navigation.navigate('Ticked',{ticked:actionData.userAction == undefined ? [] : actionData.userAction.ticked,scan_uid:actionData.scan_uid,uid:actionData.uid,app_url:actionData.app_url});
		}else if(action == 'note'){
			this.props.saveNote(actionData,undefined)
			this.props.navigation.navigate('Notes',{note:actionData.userAction == undefined ? [] : actionData.userAction.note,scan_uid:actionData.scan_uid,name:actionData.name,app_url:actionData.app_url,uid:actionData.uid});
		}else if(action == 'audio'){
			this.props.saveAudio(actionData,undefined)
			this.props.navigation.navigate('Audio',{audio:actionData.userAction == undefined ? [] : actionData.userAction.audio,scan_uid:actionData.scan_uid,uid:actionData.uid,app_url:actionData.app_url});
		}
	}

  render() {
		const lang = this.props.lang && this.props.lang.HotLeads
		let user = this.props.navigation.state.params.profile;
		let arr = [user.name,user.delegate,user.company,user.email]
		let btnFunc = [
			['ticked',lang[1]],
			['note',lang[2]],
			['audio',lang[3]]
		]

		const content = 
		  <Content>
	    	<View style={styles.container}>
	    		<Thumbnail source={{uri:user.avatar !== null ? `${baseUrl}app/${user.avatar}` :'https://cdn2.vectorstock.com/i/thumb-large/34/96/flat-busness-man-user-profile-avatar-in-suit-vector-4333496.jpg'}} style={{height: 140, width: 140,margin:15}}/>
	    		<Card style={{width:250}}>
	    			<CardItem>
	    				<Body>
	    					{arr.map((detail,index)=>(
									<Text key={index}>{detail}</Text>
	    						// <AllText color={'rgb(81, 81, 81)'} key={index} text={detail} />
	    					))}
	    				</Body>
	    			</CardItem>
	    		</Card>
	    	</View>
	    	{btnFunc.map((detail,index)=>(
	    		<AllButton key={index} navigateTo={()=>this.userAction(detail[0])} buttonTitle={detail[1]} />
	    	))}
			</Content>
			// <Text>text me.</Text>
    return (
		<AllContainer content={content} />
    );
  }
}

const styles = StyleSheet.create({
	container: {
		borderRadius:5,
	  justifyContent: 'center',
		alignItems: 'center',
		margin:15,
		marginBottom:0
	}
});

export default HotLeads;