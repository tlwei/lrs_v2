import React, { Component } from 'react';
import { Content,View,Button,Text, Container } from 'native-base';
import { Image,Dimensions,TouchableOpacity } from 'react-native';
import AllButton from './../../common/allButton';
import AllText from '../../common/allText';
import AllContainer from './../../common/allContainer';
import {baseUrl} from './../../config/constants';
import QRCode from 'react-native-qrcode';
import LoadingScreen from './../../common/loading'
class Home extends Component { 
	constructor(props){
		super(props)
		this.state={ 
			qrcode:'',
		}
	}
	
	
  render() {  
	
		const lang = this.props.lang && this.props.lang.Home
		user = this.props.user && this.props.user[0][0];
		
		let code = user && user.uid;
		let qrcode = { uri : `${baseUrl}app/${user && user.qrcode}` };
		let userLength = user && user.scanUser.length;
		const content = 
		<Container> 
			<LoadingScreen loading={this.props.fetching} />
			<View style={{alignItems: 'center',flex: 1,justifyContent: 'center'}}>
				{/* <Image source={qrcode} style={{ height:Dimensions.get('window').height/2,width: Dimensions.get('window').width}}/> */}
				<View style={{flex: 1,
        		 backgroundColor: '#0000',
        		 alignItems: 'center',
        		 justifyContent: 'center'}}> 
					<AllText text={lang[1]} />
					<Text/>
					<TouchableOpacity onPress={()=>this.props.navigation.navigate('Qrcode',{status:'scan'})}>
						{/* <QRCode
        		  value={"&ScanUser="+code}
        		  size={200}
        		  bgColor='black'
        		  fgColor='white'
						/> */}
						<Image source={require('./../../../assets/imgs/camera.jpg')} style={{width: 300, height: 300}}/>
					</TouchableOpacity>
					<Text/>
	
					<Text/>
				</View>
			</View>
			<View style={{bottom:'10%'}}>
				<AllButton navigateTo={()=>this.props.navigation.navigate('Detail')} buttonTitle={`${lang[3]} : ${userLength}`} />
			</View>
			</Container>
    return ( 
		<AllContainer content={content} />
    );
  }
}

export default Home;