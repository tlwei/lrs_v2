import React, { Component } from 'react';
import { Container,Icon,Right,Left, Header, Content, List, ListItem, Text } from 'native-base';
import ScanList from './../../common/scanList';
import AllText from '../../common/allText';
import {userData} from './../../config/constants';
import AllContainer from './../../common/allContainer'
export default class ScannedLeads extends Component {
  constructor(props){
    super(props)
    this.state={
 
    }
  }
  
  render() { 
		const lang = this.props.lang && this.props.lang.ScanLeads
    user = this.props.user && this.props.user[0][0];
    userLength = user && user.scanUser.length;
    const content=
      <Content>
        <List style={{backgroundColor:'white'}}>
          <ListItem itemDivider>
            <AllText color={'rgb(81, 81, 81)'} text={`${lang[1]}: `+ userLength}/>
          </ListItem>    
          {user && user.scanUser.map((user,index)=>(
            <ScanList 
             key={index} 
             player={user.name+', '+user.company == null ? '' : user.company} 
             navigateTo={()=>this.props.navigation.navigate('HotLeads',{profile:user})} />
          ))}
        </List>
      </Content>

    return (
      <AllContainer content={content} />
    );
  }
}